﻿TOPAMA (TOPOGRAPHIE DE L’ANTIQUITÉ ET DU MOYEN ÂGE)


Ce projet est porté par le LaMOP (Laboratoire de Médiévistique Occidentale de Paris - UMR 8589) sous la direction scientifique de Thomas Lienhard (Paris 1, LaMOP).

TOPAMA fournit des données concernant la géographie historique de l’Europe et de l’Afrique du Nord entre l’an 0 et 1500. Les données qu’il propose sont sérielles, datées, de grande précision (généralement à l’échelle de l’are, sauf pour les frontières terrioriales) et au format ouvert, ce qui signifie que quiconque peut les modifier pour les personnaliser. 

L’équipe se compose de Thomas Lienhard (Paris 1, LaMOP https://orcid.org/0000-0003-2253-7004), Pierre Brochard (IE CNRS, LaMOP https://orcid.org/0000-0003-1955-556X) et Mathieu Beaud (IR, Paris 1, LaMOP). 

Les années 2012-2019 ont été consacrées à une première collecte de données proposée ici en téléchargement libre, à savoir: 
- Les archevêchés et évêchés
- Une présentation géoréférencée des territoires des principales provinces romaines, des royaumes et empires de toutes l’Europe (occidentale, centrale, orientale), de l’espace grec et parfois de l’Afrique du nord, de l’Antiquité tardive jusqu’à l’an mille.

Durant les prochaines années, le travail s'effectuera sur deux domaines : 

- enrichissement des données (notamment pour ce qui concerne les monastères, les palais et l’Afrique du Nord)
- ouverture au public d’un WebSIG interactif hébergé par Huma-Num. 

Les cartes ont été conçues sur le logiciel Qgis. Elles sont proposées au format Shapefile et peuvent être importées comme couche sur un logiciel SIG.

Les documents sont mis à disposition selon les termes de la Licence Creative
Commons Attribution - Partage dans les Mêmes Conditions 4.0 International (https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

Contact : contact@lamop.fr

Pour citer ce projet :
Direction scientifique Thomas Lienhard (Paris 1, LaMOP), Production et classement des données, développements informatiques par Pierre Brochard (CNRS, LaMOP), Mathieu Beaud (Paris 1, LaMOP).

Notes: 
05/06/2023 : Version 2.1 : Ajout et mise à jour de cartes

17/10/2019 : Version 1.99 : Ajout d'une centaine de cartes 

05/07/2019 : Version 0.99 : Mise en ligne de la version Alpha 

